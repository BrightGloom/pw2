const path = require('path');
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const extractSass = new ExtractTextPlugin({
	filename: "style.min.css"
});

const buildPath = path.resolve(__dirname, './dist/');
const sourcePath = path.join(__dirname, './src');

module.exports = {
	entry: './src/app.js',
	output: {
		filename: 'bundle.min.js',
		path: buildPath
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: extractSass.extract({
					use: [{
						loader: "css-loader",
						options: {
							minimize: true
						}
					}, {
						loader: "sass-loader"
					}],
					// use style-loader in development
					fallback: "style-loader"
				})
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.(png|gif|jpg|svg)$/,
				use: 'file-loader?hash=sha512&name=[path][hash].[ext]',
			}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		}),
		new HtmlWebpackPlugin({
			template: path.join(sourcePath, 'index.html'),
			path: buildPath,
			filename: 'index.html',
		}),
		new webpack.optimize.UglifyJsPlugin({
			minimize: true,
			compress: {
				warnings: false,
				screw_ie8: true,
				conditionals: true,
				unused: true,
				comparisons: true,
				sequences: true,
				dead_code: true,
				evaluate: true,
				if_return: true,
				join_vars: true,
			},
			output: {
				comments: false,
			},
		}),
		extractSass
	]
};
import './scss/style.scss';

const skills = [
	{
		name: 'PHP',
		level: 1
	},
	{
		name: 'Ruby',
		level: 1
	},
	{
		name: 'javascript',
		level: 0.5
	},
	{
		name: 'actionscript',
		level: 0.1
	},
];

class Skill {
	constructor (name, level) {
		this.name = name;
		this.level = level;
	}
}

class User {
	constructor (name, address, language, skills) {
		this.name = name;
		this.address = address;
		this.language = language;
		this.skills = skills;
	}

	setName(name) {
		this.name = name;
	}
	getName() {
		return this.name;
	}
	setAddress(address) {
		this.address = address;
	}
	getAddress() {
		return this.address;
	}
	setLanguage(language) {
		this.language = language;
	}
	getLanguage() {
		return this.language;
	}
	setSkills(skills) {
		this.skills = skills;
	}
	getSkills() {
		return this.skills;
	}
}

$( document ).ready(function() {

	let user = new User();
	user.setSkills(skills);

	function edit(inputBlock, contentElement) {
		const text = $(contentElement).text(),
			inputElement = $(inputBlock.find('input')[0]);
		inputElement.val(text);
		$(contentElement).hide();
		inputBlock.show();
		inputElement.focus();
	}
	function acceptData(contentElement, inputBlock, inputElement) {
		const text = inputElement.val();
		$(contentElement).text(text);
		$(inputBlock).hide();
		$(contentElement).show();
	}
	function cancel(contentElement, inputBlock) {
		$(inputBlock).hide();
		$(contentElement).show();
	}

	function configureEditableFields() {
		$('.edit-container').each( (index, element) => {
			const editableField = $(element).find('.editable')[0],
				contentElement = $(element).find('p')[0],
				inputBlock = $($(element).find('.input-block')[0]),
				inputElement = $(inputBlock.find('input')[0]),
				saveButton = $(inputBlock.find('.save')[0]),
				cancelButton = $(inputBlock.find('.cancel')[0]);

			inputElement.keydown((event) => {
				switch (event.keyCode) {
					case 13:
						acceptData(contentElement, inputBlock, inputElement);
						break;
					case 27:
						cancel(contentElement, inputBlock);
						break;
				}
			});
			$(editableField).click(() => {
				edit(inputBlock, contentElement);
			});
			$(saveButton).click(() => {
				acceptData(contentElement, inputBlock, inputElement);
			});
			$(cancelButton).click(() => {
				cancel(contentElement, inputBlock);
			})
		});
	}


	function configureSkills() {
		const skillBlock = $('.skill-block'),
			skillsBlock = $('#skills'),
			inputElement = $(skillBlock.find('input')[0]),
			addSkillsElement = $(skillsBlock.find('p')[0]),
			acceptBtn = $(skillBlock.find('.save')[0]);


		acceptBtn.click(() => {
			saveSkill(skillBlock, addSkillsElement, inputElement);
		});
		inputElement.keydown((event) => {
			switch (event.keyCode) {
				case 13:
					saveSkill(skillBlock, addSkillsElement, inputElement);
					break;
				case 27:
					inputElement.val('');
					addSkillsElement.show();
					skillBlock.hide();
					break;
			}
		});
		user.getSkills().forEach( (skill) => {
			addSkill(skill);
		});
		addSkillsElement.click(() => {
			addSkillsElement.hide();
			skillBlock.show();
		})
	}

	function saveSkill(skillBlock, addSkillsElement, inputElement) {
		const selectElement = $(skillBlock.find('select')[0]),
			skillName = inputElement.val(),
			skillLevel = selectElement.val(),
			skill = createSkill(skillName, skillLevel);
		if (inputElement.val().trim() !== '') {
			addSkill(skill);
			inputElement.val('');
			addSkillsElement.show();
			skillBlock.hide();
		}
	}

	function createSkill(name, level) {
		return new Skill(name, level);
	}

	function addSkill(skill) {
		const skillElement = $('<div class="skill"></div>'),
			skillName = $('<div class="name"></div>'),
			removeBtn = $('<button class="remove"></button>');
		skillName.text(skill.name);
		skillName.css({'opacity': skill.level});
		$('#skills .list').append(skillElement);
		skillElement.append(removeBtn);
		skillElement.append(skillName);
		removeBtn.click(removeSkill);
	}

	function removeSkill () {
		$(this).parent().remove();
	}


	configureEditableFields();
	configureSkills();


});